from pascalanalyzer.pascalrun import PascalRun
from pascalanalyzer.sensors.fingerprint import FingerprintSample
from performance_features import get_event_description
from ipmi import IPMI
from pathlib import Path
from typing import List, Dict
import logging
import os
import sys
import contextlib

def run_pascal(run_counter : int, 
             applications : List[str],
             applications_args : Dict[List[str]],
             cores : List[int],
             freqs : List[int],
             result_path : Path, 
             **pascal_kargs : Dict[any]):
    """
        Helper function to run multiple measurements simultaneously with 
        support to resume and continue.
    """
    if not result_path.exists():
        result_path.mkdir()

    # If the measurement is interrupted, partial data is stored here.
    incomplete_path = (result_path / Path("incomplete"))
    if not incomplete_path.exists():
        incomplete_path.mkdir()

    for app in applications:
        result_file = f"{app}_{run_counter}.json"
        # Skip already measured apps
        if any([result_file in x.name for x in result_path.iterdir()]): 
            continue
        logging.info(f"Running {app}")
        with open(result_path / result_file, "wb+") as f:
            f.write(b"running")

        logging.info("Changing directory", app+"_")
        with contextlib.chdir(app+"_"):
            app = PascalRun(application = f"./{app}",
                            **pascal_kargs)
            try:
                app.run(cores = cores,
                        frequencies = freqs,
                        inputs = applications_args[app]
                )
                logging.info(f"Success {app}")
                app.save(str(result_path / result_file))
            except Exception as e:
                os.remove(str(result_path / result_file))
                logging.error("Error ocurred while executing", app)
                logging.error(e)
                app.save(str(incomplete_path / result_file))

# Sensors wrappers
class IPMIWrapper(IPMI):
    def __init__(self,*args, **kargs):
        self.sensor_name= "ipmi"
        super().__init__(*args,**kargs)
    
    def get_data(self):
        data= IPMI.get_data(self)
        return data["sources"][0]["dcOutPower"]+\
                data["sources"][1]["dcOutPower"]

### Perforamnce counters events ###
ALL_EVENTS = get_event_description()
RAPL_EVENTS = ['SYSTEMWIDE:'+e[0] for e in ALL_EVENTS if 'RAPL' in e[0]]
HW_EVENTS = ['PERF_COUNT_HW_INSTRUCTIONS','PERF_COUNT_HW_BRANCH_INSTRUCTIONS', 
            'PERF_COUNT_HW_BRANCH_MISSES', 'PERF_COUNT_HW_CACHE_MISSES','ARITH:DIVIDER_UOPS']
MEM_EVENTS = ['PERF_COUNT_HW_INSTRUCTIONS','MEM_UOPS_RETIRED:ALL_LOADS', 
                'MEM_UOPS_RETIRED:ALL_STORES']
SW_EVENTS = ['PERF_COUNT_SW_CPU_CLOCK','PERF_COUNT_SW_PAGE_FAULTS',
             'PERF_COUNT_SW_CONTEXT_SWITCHES','PERF_COUNT_SW_CPU_MIGRATIONS',
             'PERF_COUNT_SW_PAGE_FAULTS_MAJ']
TO_MONITOR = MEM_EVENTS+SW_EVENTS+RAPL_EVENTS

### Application configuration ###
BASE_PATH = sys.argv[1] # "/home/vitornpad/energy_research/energy/examples/apps/"
APPLICATIONS = ["vips", "openmc", "xhpl", "canneal", "dedup", "ferret", "x264", 
        "blackscholes", "fluidanimate", "swaptions", "rtview", "bodytrack", 
        "freqmine"
]
APPLICATIONS_ARGS = {
    "blackscholes" : [
        "__nt__ in_5_10.txt output.txt",
        "__nt__ in_6_10.txt output.txt",
        "__nt__ in_7_10.txt output.txt",
        "__nt__ in_8_10.txt output.txt",
        "__nt__ in_9_10.txt output.txt"
    ],
    "canneal": [
        "__nt__ 15000 2000 2500000.nets 3600",
        "__nt__ 15000 2000 2500000.nets 4200",
        "__nt__ 15000 2000 2500000.nets 4800",
        "__nt__ 15000 2000 2500000.nets 5400",
        "__nt__ 15000 2000 2500000.nets 6000"
    ],
    "dedup": [
        "-c -p -v -t __nt__ -i FC-6-x86_64-disc1_05.iso -o output.dat.ddp",
        "-c -p -v -t __nt__ -i FC-6-x86_64-disc1_06.iso -o output.dat.ddp",
        "-c -p -v -t __nt__ -i FC-6-x86_64-disc1_07.iso -o output.dat.ddp",
        "-c -p -v -t __nt__ -i FC-6-x86_64-disc1_08.iso -o output.dat.ddp",
        "-c -p -v -t __nt__ -i FC-6-x86_64-disc1_09.iso -o output.dat.ddp"
    ],
    "ferret":[
        "corel_5 lsh queries_5 10 20 __nt__ output.txt",
        "corel_6 lsh queries_6 10 20 __nt__ output.txt",
        "corel_7 lsh queries_7 10 20 __nt__ output.txt",
        "corel_8 lsh queries_8 10 20 __nt__ output.txt",
        "corel_9 lsh queries_9 10 20 __nt__ output.txt"
    ],
    "fluidanimate": [
        "__nt__ 200 in_500K.fluid out.fluid",
        "__nt__ 300 in_500K.fluid out.fluid",
        "__nt__ 400 in_500K.fluid out.fluid",
        "__nt__ 500 in_500K.fluid out.fluid",
        "__nt__ 600 in_500K.fluid out.fluid"
    ],
    "freqmine":[
        "webdocs_250k_05.dat 11000",
        "webdocs_250k_06.dat 11000",
        "webdocs_250k_07.dat 11000",
        "webdocs_250k_08.dat 11000",
        "webdocs_250k_09.dat 11000"
    ],
    "rtview": [
        "thai_statue.obj -automove -nthreads __nt__ -frames 10000 -res 346 346",
        "thai_statue.obj -automove -nthreads __nt__ -frames 10000 -res 374 374",
        "thai_statue.obj -automove -nthreads __nt__ -frames 10000 -res 400 400",
        "thai_statue.obj -automove -nthreads __nt__ -frames 10000 -res 424 424",
        "thai_statue.obj -automove -nthreads __nt__ -frames 10000 -res 450 450"
    ],
    "swaptions":[
        "-ns 32 -sm 2000000 -nt __nt__",
        "-ns 32 -sm 3000000 -nt __nt__",
        "-ns 32 -sm 4000000 -nt __nt__",
        "-ns 32 -sm 5000000 -nt __nt__",
        "-ns 32 -sm 6000000 -nt __nt__",
    ],
    "vips":[
        "im_benchmark orion_10800x10800.v output.v",
        "im_benchmark orion_12600x12600.v output.v",
        "im_benchmark orion_14400x14400.v output.v",
        "im_benchmark orion_16200x16200.v output.v",
        "im_benchmark orion_18000x18000.v output.v"
    ],
    "x264":[
        "--quiet --qp 20 --partitions b8x8,i4x4 --ref 5 --direct auto --b-pyramid --weightb --mixed-refs --no-fast-pskip --me umh --subme 7 --analyse b8x8,i4x4 --threads __nt__ -o eledream.264 eledream_1920x1080_306.y4m",
        "--quiet --qp 20 --partitions b8x8,i4x4 --ref 5 --direct auto --b-pyramid --weightb --mixed-refs --no-fast-pskip --me umh --subme 7 --analyse b8x8,i4x4 --threads __nt__ -o eledream.264 eledream_1920x1080_357.y4m",
        "--quiet --qp 20 --partitions b8x8,i4x4 --ref 5 --direct auto --b-pyramid --weightb --mixed-refs --no-fast-pskip --me umh --subme 7 --analyse b8x8,i4x4 --threads __nt__ -o eledream.264 eledream_1920x1080_408.y4m",
        "--quiet --qp 20 --partitions b8x8,i4x4 --ref 5 --direct auto --b-pyramid --weightb --mixed-refs --no-fast-pskip --me umh --subme 7 --analyse b8x8,i4x4 --threads __nt__ -o eledream.264 eledream_1920x1080_459.y4m",
        "--quiet --qp 20 --partitions b8x8,i4x4 --ref 5 --direct auto --b-pyramid --weightb --mixed-refs --no-fast-pskip --me umh --subme 7 --analyse b8x8,i4x4 --threads __nt__ -o eledream.264 eledream_1920x1080_512.y4m"
    ],
    "xhpl":[
        "__nt__ 7000",
        "__nt__ 8000",
        "__nt__ 9000",
        "__nt__ 10000",
        "__nt__ 11000"
    ],
    "openmc":[
        "input1",
        "input2",
        "input3",
        "input4",
        "input5"
    ],
    "bodytrack":[
        "sequenceB_1043 4 204 1000 10 0 __nt__",
        "sequenceB_1043 4 408 1000 10 0 __nt__",
        "sequenceB_1043 4 612 1000 10 0 __nt__",
        "sequenceB_1043 4 816 1000 10 0 __nt__",
        "sequenceB_1043 4 1020 1000 10 0 __nt__"
    ]
}

logging.info("Changing directory", BASE_PATH)
os.chdir(BASE_PATH)

IPMI = IPMIWrapper(server="http://localhost:8080", 
                    user="admin", password="admin")
PERFORMANCE_COUNTERS_SENSOR = FingerprintSample(counters = TO_MONITOR)
CORES = [1]+list(range(2, 32, 2))
FREQUENCIES = [2200000, 2000000, 1800000, 1600000, 1400000, 1200000]

### Collect energy consumption with fixed cores and frequency for all applications ###
RESULT_PATH = Path("results/fixed")
run_pascal(run_counter = 1, 
        applications = APPLICATIONS,
        applications_args = APPLICATIONS_ARGS,
        cores = CORES,
        freqs = FREQUENCIES,
        result_path = RESULT_PATH,
        repetitions = 3,
        sensors = [IPMI, PERFORMANCE_COUNTERS_SENSOR],
        idle_time = 5,
        identify_regions = True,
        governor = "userspace",
        disable_cores = True,
        disable_hyperthread = True,
        instr_auto = True,
        verbose = 2
)

### Collect energy consumption with ondemand for all applications ###
RESULT_PATH = Path("results/ondemand")
run_pascal(run_counter = 1, 
        applications = APPLICATIONS,
        applications_args = APPLICATIONS_ARGS,
        cores = CORES,
        freqs = None,
        result_path = RESULT_PATH,
        repetitions = 3,
        sensors = [IPMI, PERFORMANCE_COUNTERS_SENSOR],
        idle_time = 5,
        identify_regions = False,
        governor = "ondemand",
        disable_cores = True,
        disable_hyperthread = True,
        instr_auto = False,
        verbose = 2
)