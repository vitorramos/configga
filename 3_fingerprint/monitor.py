from performance_features import Profiler, run_program, get_event_description
from tqdm import tqdm
import pandas as pd
import numpy as np
import os, pickle

flat_list= lambda x: [ g for f in x for g in f ]
double_list= lambda x: [[g] for g in x]
split_n= lambda x, n: [x[i:i + n] for i in range(0, len(x), n)]


BASE_PATH = "/home/vitornpad/energy_research/energy/examples/apps/"
APPLICATIONS = ["vips", "openmc", "xhpl", "canneal", "dedup", "ferret", "x264",
        "blackscholes", "fluidanimate", "swaptions", "rtview", "bodytrack",
        "freqmine"
]

APPLICATIONS_ARGS = {
    "blackscholes" : [
        "__nt__ in_5_10.txt output.txt",
        "__nt__ in_6_10.txt output.txt",
        "__nt__ in_7_10.txt output.txt",
        "__nt__ in_8_10.txt output.txt",
        "__nt__ in_9_10.txt output.txt"
    ],
    "canneal": [
        "__nt__ 15000 2000 2500000.nets 3600",
        "__nt__ 15000 2000 2500000.nets 4200",
        "__nt__ 15000 2000 2500000.nets 4800",
        "__nt__ 15000 2000 2500000.nets 5400",
        "__nt__ 15000 2000 2500000.nets 6000"
    ],
    "dedup": [
        "-c -p -v -t __nt__ -i FC-6-x86_64-disc1_05.iso -o output.dat.ddp",
        "-c -p -v -t __nt__ -i FC-6-x86_64-disc1_06.iso -o output.dat.ddp",
        "-c -p -v -t __nt__ -i FC-6-x86_64-disc1_07.iso -o output.dat.ddp",
        "-c -p -v -t __nt__ -i FC-6-x86_64-disc1_08.iso -o output.dat.ddp",
        "-c -p -v -t __nt__ -i FC-6-x86_64-disc1_09.iso -o output.dat.ddp"
    ],
    "ferret":[
        "corel_5 lsh queries_5 10 20 __nt__ output.txt",
        "corel_6 lsh queries_6 10 20 __nt__ output.txt",
        "corel_7 lsh queries_7 10 20 __nt__ output.txt",
        "corel_8 lsh queries_8 10 20 __nt__ output.txt",
        "corel_9 lsh queries_9 10 20 __nt__ output.txt"
    ],
    "fluidanimate": [
        "__nt__ 200 in_500K.fluid out.fluid",
        "__nt__ 300 in_500K.fluid out.fluid",
        "__nt__ 400 in_500K.fluid out.fluid",
        "__nt__ 500 in_500K.fluid out.fluid",
        "__nt__ 600 in_500K.fluid out.fluid"
    ],
    "freqmine":[
        "webdocs_250k_05.dat 11000",
        "webdocs_250k_06.dat 11000",
        "webdocs_250k_07.dat 11000",
        "webdocs_250k_08.dat 11000",
        "webdocs_250k_09.dat 11000"
    ],
    "rtview": [
        "thai_statue.obj -automove -nthreads __nt__ -frames 10000 -res 346 346",
        "thai_statue.obj -automove -nthreads __nt__ -frames 10000 -res 374 374",
        "thai_statue.obj -automove -nthreads __nt__ -frames 10000 -res 400 400",
        "thai_statue.obj -automove -nthreads __nt__ -frames 10000 -res 424 424",
        "thai_statue.obj -automove -nthreads __nt__ -frames 10000 -res 450 450"
    ],
    "swaptions":[
        "-ns 32 -sm 2000000 -nt __nt__",
        "-ns 32 -sm 3000000 -nt __nt__",
        "-ns 32 -sm 4000000 -nt __nt__",
        "-ns 32 -sm 5000000 -nt __nt__",
        "-ns 32 -sm 6000000 -nt __nt__",
    ],
    "vips":[
        "im_benchmark orion_10800x10800.v output.v",
        "im_benchmark orion_12600x12600.v output.v",
        "im_benchmark orion_14400x14400.v output.v",
        "im_benchmark orion_16200x16200.v output.v",
        "im_benchmark orion_18000x18000.v output.v"
    ],
    "x264":[
        "--quiet --qp 20 --partitions b8x8,i4x4 --ref 5 --direct auto --b-pyramid --weightb --mixed-refs --no-fast-pskip --me umh --subme 7 --analyse b8x8,i4x4 --threads __nt__ -o eledream.264 eledream_1920x1080_306.y4m",
        "--quiet --qp 20 --partitions b8x8,i4x4 --ref 5 --direct auto --b-pyramid --weightb --mixed-refs --no-fast-pskip --me umh --subme 7 --analyse b8x8,i4x4 --threads __nt__ -o eledream.264 eledream_1920x1080_357.y4m",
        "--quiet --qp 20 --partitions b8x8,i4x4 --ref 5 --direct auto --b-pyramid --weightb --mixed-refs --no-fast-pskip --me umh --subme 7 --analyse b8x8,i4x4 --threads __nt__ -o eledream.264 eledream_1920x1080_408.y4m",
        "--quiet --qp 20 --partitions b8x8,i4x4 --ref 5 --direct auto --b-pyramid --weightb --mixed-refs --no-fast-pskip --me umh --subme 7 --analyse b8x8,i4x4 --threads __nt__ -o eledream.264 eledream_1920x1080_459.y4m",
        "--quiet --qp 20 --partitions b8x8,i4x4 --ref 5 --direct auto --b-pyramid --weightb --mixed-refs --no-fast-pskip --me umh --subme 7 --analyse b8x8,i4x4 --threads __nt__ -o eledream.264 eledream_1920x1080_512.y4m"
    ],
    "xhpl":[
        "__nt__ 7000",
        "__nt__ 8000",
        "__nt__ 9000",
        "__nt__ 10000",
        "__nt__ 11000"
    ],
    "openmc":[
        "input1",
        "input2",
        "input3",
        "input4",
        "input5"
    ],
    "bodytrack":[
        "sequenceB_1043 4 204 1000 10 0 __nt__",
        "sequenceB_1043 4 408 1000 10 0 __nt__",
        "sequenceB_1043 4 612 1000 10 0 __nt__",
        "sequenceB_1043 4 816 1000 10 0 __nt__",
        "sequenceB_1043 4 1020 1000 10 0 __nt__"
    ]
}


def run_parsec(to_monitor):
    for prog in APPLICATIONS:
        ppath = os.path.join(BASE_PATH, prog+"_")
        os.chdir(ppath)
        reset_on_sample= False
        sample_period= 0.05
        args = APPLICATIONS_ARGS[prog][-1].split(" ")
        print([ppath+"/"+prog]+args)
        data= run_program(pargs=[ppath+"/"+prog]+args,
                    to_monitor=to_monitor,n=3,
                    reset_on_sample=reset_on_sample,
                    sample_period=sample_period)
        with open('{}_mem.dat'.format(prog),'wb+') as f:
            pickle.dump(data, f)

# Interrest
all_events= get_event_description()
rapl_events=['SYSTEMWIDE:'+e[0] for e in all_events if 'RAPL' in e[0]]
hw_events= [['PERF_COUNT_HW_INSTRUCTIONS','PERF_COUNT_HW_BRANCH_INSTRUCTIONS', 'PERF_COUNT_HW_BRANCH_MISSES', 'PERF_COUNT_HW_CACHE_MISSES','ARITH:DIVIDER_UOPS']]
mem_events= [['PERF_COUNT_HW_INSTRUCTIONS','MEM_UOPS_RETIRED:ALL_LOADS', 'MEM_UOPS_RETIRED:ALL_STORES']]
sw_events= [['PERF_COUNT_SW_CPU_CLOCK','PERF_COUNT_SW_PAGE_FAULTS','PERF_COUNT_SW_CONTEXT_SWITCHES',
                       'PERF_COUNT_SW_CPU_MIGRATIONS','PERF_COUNT_SW_PAGE_FAULTS_MAJ']]
rapl_events= double_list(rapl_events)

to_monitor= mem_events+sw_events+rapl_events
run_parsec(to_monitor)