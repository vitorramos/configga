import json
import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
import os

def find_best(df_fingerprints, df_phases, nphases = 4):
    df_fingerprints["fp"] = df_fingerprints["fp"].apply(lambda a: np.array(a[1:-1].split(), dtype=float))
    
    min_err = 99999
    for _, p in df_fingerprints.iterrows():
        dif = np.diff(p["fp"])
        dif = np.diff(dif)
        dif = abs(dif)
        idx = np.where(dif > dif.mean())[0]+1
        X = np.hstack((idx, p["fp"][idx])).reshape((-1, 2), order='F')

        if len(X) < 2: continue

        km = KMeans(n_clusters=nphases-2, random_state=0).fit(X)
        centers = km.cluster_centers_
        centers = np.vstack((centers, [0, p["fp"][0]]))
        centers = np.vstack((centers, [99, p["fp"][99]]))
        centers = centers[centers[:, 0].argsort()]

        df_phases_ = df_phases[df_phases["nphases"] == nphases]
        df_phases_ = df_phases_["phases"].values[-1]
        df_phases_ = json.loads(df_phases_.replace("'", "\""))
        x = [float(y)*100 for y in df_phases_]

        err = sum(abs((centers[:, 0])-x))
        if err < min_err:
            min_p = p
            min_err = err

    return list(min_p.values)

df_phases = pd.read_csv("app_phases.csv", index_col=0)
df_phases = df_phases[df_phases["app"].str.contains("app")]
df_fingerprints = pd.read_csv("app_fingerprint.csv", index_col=0)
find_best(df_fingerprints, df_phases)