from performance_features import Analyser
import pandas as pd
import glob
import os
from tqdm import tqdm
from itertools import product

operations = ["+","-","/","*",""]
ppath = "results"
nphases = 4

for f in glob.glob(f"{ppath}/*.dat"):
    fp_data = Analyser(f)
    aname = os.path.basename(f)[:-8]

    aa = []
    for fet in fp_data.df.columns:
        try:
            fp_data.interpolate(feature=fet, npoints=100)
        except:
            aa+=[fet]
    cols = set(fp_data.df.columns)-set(aa)
    cols, aa
    
    fingerprints = []
    a = list(product(cols, repeat=nphases))
    for c in tqdm(a):
        for p in product(operations, repeat=nphases-1):
            cc = c
            _, res = fp_data.interpolate(feature=c[0], npoints=100)
            for pp in p:
                cc = cc[1:]
                _, y0 = fp_data.interpolate(feature=cc[0], npoints=100)

                if pp == "+":
                    res += y0
                if pp == "-":
                    res -= y0
                if pp == "*":
                    res *= y0
                if pp == "/":
                    res /= y0
            fingerprints.append([aname, p, c, res])
    df_fingerprints = pd.DataFrame(fingerprints, columns=["app", "operations", "counters", "fp"])
    df_fingerprints.to_csv(f"fingerprints_{aname}.csv")