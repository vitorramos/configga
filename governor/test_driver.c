#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>

#include <linux/perf_event.h>

static DEFINE_PER_CPU(struct perf_event*, ev_inst);
static DEFINE_PER_CPU(struct perf_event*, ev_mem);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Robert W.Oliver II");
MODULE_DESCRIPTION("A simple example Linux module.");
MODULE_VERSION("0.01");


static void create_events(void)
{
	struct perf_event_attr pe;
	memset(&pe, 0, sizeof(struct perf_event_attr));
	pe.size = sizeof(struct perf_event_attr);
	pe.disabled = 0;
	pe.exclude_kernel = 1;
	pe.exclude_hv = 1;

	int cpu;
	get_online_cpus();
	for_each_online_cpu(cpu)
	{
		pe.type = PERF_TYPE_HARDWARE;
		pe.config = PERF_COUNT_HW_INSTRUCTIONS;
		struct perf_event *ev_i = per_cpu(ev_inst, cpu);
		ev_i = perf_event_create_kernel_counter(&pe, cpu, NULL, NULL, NULL);
		// printk(KERN_INFO"Creating event %p %d", ev_i, cpu);
		if(IS_ERR(ev_i))
		{
			pr_debug("Perf event create on CPU %d failed with %ld\n", 0, PTR_ERR(ev_i));
			return PTR_ERR(ev_i);
		}
		per_cpu(ev_inst, cpu) = ev_i;

		pe.type = PERF_TYPE_RAW;
		pe.config = 0x5381d0;
		struct perf_event *ev_m = per_cpu(ev_mem, cpu);
		ev_m = perf_event_create_kernel_counter(&pe, cpu, NULL, NULL, NULL);
		// printk(KERN_INFO"Creating event %p %d", ev_m, cpu);
		if(IS_ERR(ev_m))
		{
			pr_debug("Perf event create on CPU %d failed with %ld\n", 0, PTR_ERR(ev_m));
			return PTR_ERR(ev_m);
		}
		per_cpu(ev_mem, cpu) = ev_m;
	}
	put_online_cpus();
}

static void destroy_events(void)
{
	int cpu;
	u64 c_i, c_m, enabled, running;
	get_online_cpus();
	for_each_online_cpu(cpu)
	{
		struct perf_event *ev_i = per_cpu(ev_inst, cpu);
		// printk(KERN_INFO"Destroying event %p %d", ev_i, cpu);
		c_i += perf_event_read_value(ev_i, &enabled, &running);
		if(ev_i)
			perf_event_release_kernel(ev_i);

		struct perf_event *ev_m = per_cpu(ev_mem, cpu);
		// printk(KERN_INFO"Destroying event %p %d", ev_m, cpu);
		c_m += perf_event_read_value(ev_m, &enabled, &running);
		if(ev_m)
			perf_event_release_kernel(ev_m);
	}
	put_online_cpus();
	printk(KERN_INFO "Total instructions = %lld\n", c_i);
	printk(KERN_INFO "Total memory acess = %lld\n", c_m);
}

static int __init lkm_example_init(void)
{
    create_events();
    return 0;
}

static void __exit lkm_example_exit(void)
{
    destroy_events();
}

module_init(lkm_example_init);
module_exit(lkm_example_exit);