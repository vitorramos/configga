// Copyright (c) 2007 Intel Corp.

// Black-Scholes
// Analytical method for calculating European Options
//
//
// Reference Source: Options, Futures, and Other Derivatives, 3rd Edition, Prentice
// Hall, John C. Hull,

#include <vector>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <numeric>
#include <math.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <chrono>
#include <algorithm>

using namespace std;
using namespace chrono;


#include <pascalops.h>
//Precision to use for calculations
#define fptype float

#define NUM_RUNS 100

typedef struct OptionData_
{
    fptype s;        // spot price
    fptype strike;   // strike price
    fptype r;        // risk-free interest rate
    fptype divq;     // dividend rate
    fptype v;        // volatility
    fptype t;        // time to maturity or option expiration in years
                     //     (1yr = 1.0, 6mos = 0.5, 3mos = 0.25, ..., etc)
    char OptionType; // Option type.  "P"=PUT, "C"=CALL
    fptype divs;     // dividend vals (not used in this test)
    fptype DGrefval; // DerivaGem Reference Value
} OptionData;

OptionData *data;
fptype *prices;
int numOptions;

int *otype;
fptype *sptprice;
fptype *strike;
fptype *rate;
fptype *volatility;
fptype *otime;
int numError = 0;
int nThreads;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Cumulative Normal Distribution Function
// See Hull, Section 11.8, P.243-244
#define inv_sqrt_2xPI 0.39894228040143270286

fptype CNDF(fptype InputX)
{
    int sign;

    fptype OutputX;
    fptype xInput;
    fptype xNPrimeofX;
    fptype expValues;
    fptype xK2;
    fptype xK2_2, xK2_3;
    fptype xK2_4, xK2_5;
    fptype xLocal, xLocal_1;
    fptype xLocal_2, xLocal_3;

    // Check for negative value of InputX
    if (InputX < 0.0)
    {
        InputX = -InputX;
        sign = 1;
    }
    else
        sign = 0;

    xInput = InputX;

    // Compute NPrimeX term common to both four & six decimal accuracy calcs
    expValues = exp(-0.5f * InputX * InputX);
    xNPrimeofX = expValues;
    xNPrimeofX = xNPrimeofX * inv_sqrt_2xPI;

    xK2 = 0.2316419 * xInput;
    xK2 = 1.0 + xK2;
    xK2 = 1.0 / xK2;
    xK2_2 = xK2 * xK2;
    xK2_3 = xK2_2 * xK2;
    xK2_4 = xK2_3 * xK2;
    xK2_5 = xK2_4 * xK2;

    xLocal_1 = xK2 * 0.319381530;
    xLocal_2 = xK2_2 * (-0.356563782);
    xLocal_3 = xK2_3 * 1.781477937;
    xLocal_2 = xLocal_2 + xLocal_3;
    xLocal_3 = xK2_4 * (-1.821255978);
    xLocal_2 = xLocal_2 + xLocal_3;
    xLocal_3 = xK2_5 * 1.330274429;
    xLocal_2 = xLocal_2 + xLocal_3;

    xLocal_1 = xLocal_2 + xLocal_1;
    xLocal = xLocal_1 * xNPrimeofX;
    xLocal = 1.0 - xLocal;

    OutputX = xLocal;

    if (sign)
    {
        OutputX = 1.0 - OutputX;
    }

    return OutputX;
}

//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
fptype BlkSchlsEqEuroNoDiv(fptype sptprice,
                           fptype strike, fptype rate, fptype volatility,
                           fptype time, int otype, float timet)
{
    fptype OptionPrice;

    // local private working variables for the calculation
    fptype xStockPrice;
    fptype xStrikePrice;
    fptype xRiskFreeRate;
    fptype xVolatility;
    fptype xTime;
    fptype xSqrtTime;

    fptype logValues;
    fptype xLogTerm;
    fptype xD1;
    fptype xD2;
    fptype xPowerTerm;
    fptype xDen;
    fptype d1;
    fptype d2;
    fptype FutureValueX;
    fptype NofXd1;
    fptype NofXd2;
    fptype NegNofXd1;
    fptype NegNofXd2;

    xStockPrice = sptprice;
    xStrikePrice = strike;
    xRiskFreeRate = rate;
    xVolatility = volatility;

    xTime = time;
    xSqrtTime = sqrt(xTime);

    logValues = log(sptprice / strike);

    xLogTerm = logValues;

    xPowerTerm = xVolatility * xVolatility;
    xPowerTerm = xPowerTerm * 0.5;

    xD1 = xRiskFreeRate + xPowerTerm;
    xD1 = xD1 * xTime;
    xD1 = xD1 + xLogTerm;

    xDen = xVolatility * xSqrtTime;
    xD1 = xD1 / xDen;
    xD2 = xD1 - xDen;

    d1 = xD1;
    d2 = xD2;

    NofXd1 = CNDF(d1);
    NofXd2 = CNDF(d2);

    FutureValueX = strike * (exp(-(rate) * (time)));
    if (otype == 0)
    {
        OptionPrice = (sptprice * NofXd1) - (FutureValueX * NofXd2);
    }
    else
    {
        NegNofXd1 = (1.0 - NofXd1);
        NegNofXd2 = (1.0 - NofXd2);
        OptionPrice = (FutureValueX * NegNofXd2) - (sptprice * NegNofXd1);
    }

    return OptionPrice;
}

int bs_thread(void *tid_ptr)
{
    int i, j;
    fptype price;
    fptype priceDelta;
    int tid = *(int *)tid_ptr;
    int start = tid * (numOptions / nThreads);
    int end = start + (numOptions / nThreads);

    for (j = 0; j < NUM_RUNS; j++)
    {
#pragma omp parallel for private(i, price, priceDelta)
        for (i = 0; i < numOptions; i++)
        {
            /* Calling main function to calculate option value based on 
             * Black & Scholes's equation.
             */
            price = BlkSchlsEqEuroNoDiv(sptprice[i], strike[i],
                                        rate[i], volatility[i], otime[i],
                                        otype[i], 0);
            prices[i] = price;

#ifdef ERR_CHK
            priceDelta = ::data[i].DGrefval - price;
            if (fabs(priceDelta) >= 1e-4)
            {
                printf("Error on %d. Computed=%.5f, Ref=%.5f, Delta=%.5f\n",
                       i, price, ::data[i].DGrefval, priceDelta);
                numError++;
            }
#endif
        }
    }

    return 0;
}

template <typename T>
vector<vector<T>> cartesian(vector<vector<T>> &v)
{
    long N = 1;
    for (auto &a : v)
        N *= a.size();
    vector<vector<T>> res;
    for (long long n = 0; n < N; ++n)
    {
        vector<T> u(v.size());
        lldiv_t q{n, 0};
        for (long long i = v.size() - 1; 0 <= i; --i)
        {
            q = div(q.quot, v[i].size());
            u[i] = v[i][q.rem];
        }
        res.push_back(u);
        // for (T x : u)
        //     cout << x << ' ';
        // cout << '\n';
    }
    return res;
}

int main(int argc, char **argv)
{
    vector<int> freqs = {3000000, 2800000, 2600000, 2400000,
                         2200000, 2000000, 1800000, 1600000,
                         1400000, 1200000};
    vector<int> cores = {1, 2, 3, 4};
    vector<vector<int>> l = {freqs, cores};
    auto comb = cartesian(l);
    vector<vector<vector<int>>> l2 = {comb, comb, comb};
    auto combs = cartesian(l2);
    // for(auto& phase: combs){
    //     string cmd;
    //     int f, c;

    //     f= phase[0][0];
    //     c= phase[0][1];
    //     cmd= "./cpu_control.py --cores "+to_string(c)+" --gov userspace --freq "+to_string(f);
    //     system(cmd.c_str());

    //     f= phase[1][0];
    //     c= phase[1][1];
    //     cmd= "./cpu_control.py --cores "+to_string(c)+" --gov userspace --freq "+to_string(f);
    //     system(cmd.c_str());

    //     f= phase[2][0];
    //     c= phase[2][1];
    //     cmd= "./cpu_control.py --cores "+to_string(c)+" --gov userspace --freq "+to_string(f);
    //     system(cmd.c_str());
    // }
    // return 0;
    ofstream table("result.csv", ofstream::app);
    table << "idx,phase,time,energy,tt_time,tt_energy" << endl;
    const string rapl_path= "/sys/class/powercap/intel-rapl/intel-rapl:0/energy_uj";
    ifstream rapl;
    string cmd, t_phase, t_time, t_energy, tt_time, tt_energy;
    vector<vector<int>> p1_discard;
    vector<vector<int>> p2_discard;

    double eni, enf, enti, entf;
    double en_min= 9999;
    int f, c, nphase = 0;
    for (auto &phase : combs)
    {
        bool jump_p= false;
        for(auto& p1d: p1_discard){
            if(p1d[0] == phase[0][0] &&
                p1d[1] == phase[0][1]
            ){
                jump_p= true;
            }
        }
        for(auto& p2d: p2_discard){
            if(p2d[0] == phase[0][0]  &&
                p2d[1] == phase[0][1] &&
                p2d[2] == phase[1][0] &&
                p2d[3] == phase[1][1]
            ){
                jump_p= true;
            }
        }
        if(jump_p)
            continue;
        rapl.open(rapl_path);
        rapl >> enti;
        rapl.close();
        auto tti = chrono::high_resolution_clock::now();

        f = phase[0][0];
        c = phase[0][1];

        t_phase = "((" + to_string(f) + ", " + to_string(c) + "), ";
        cmd = "./cpu_control.py --cores " + to_string(c) + " --gov userspace --freq " + to_string(f);
        system(cmd.c_str());

        rapl.open(rapl_path);
        rapl >> eni;
        rapl.close();
        auto ti = chrono::high_resolution_clock::now();
        pascal_start(0);
        FILE *file;
        int i;
        int loopnum;
        fptype *buffer;
        int *buffer2;
        int rv;

        printf("PARSEC Benchmark Suite\n");
        fflush(NULL);

        if (argc != 4)
        {
            printf("Usage:\n\t%s <nthreads> <inputFile> <outputFile>\n", argv[0]);
            exit(1);
        }
        nThreads = atoi(argv[1]);
        char *inputFile = argv[2];
        char *outputFile = argv[3];

        //Read input data from file
        file = fopen(inputFile, "r");
        if (file == NULL)
        {
            printf("ERROR: Unable to open file `%s'.\n", inputFile);
            exit(1);
        }
        rv = fscanf(file, "%i", &numOptions);
        if (rv != 1)
        {
            printf("ERROR: Unable to read from file `%s'.\n", inputFile);
            fclose(file);
            exit(1);
        }
        if (nThreads > numOptions)
        {
            printf("WARNING: Not enough work, reducing number of threads to match number of options.\n");
            nThreads = numOptions;
        }

        // alloc spaces for the option data
        ::data = (OptionData *)malloc(numOptions * sizeof(OptionData));
        prices = (fptype *)malloc(numOptions * sizeof(fptype));
        for (loopnum = 0; loopnum < numOptions; ++loopnum)
        {
            rv = fscanf(file, "%f %f %f %f %f %f %c %f %f", &::data[loopnum].s, &::data[loopnum].strike, &::data[loopnum].r, &::data[loopnum].divq, &::data[loopnum].v, &::data[loopnum].t, &::data[loopnum].OptionType, &::data[loopnum].divs, &::data[loopnum].DGrefval);
            if (rv != 9)
            {
                printf("ERROR: Unable to read from file `%s'.\n", inputFile);
                fclose(file);
                exit(1);
            }
        }
        rv = fclose(file);
        if (rv != 0)
        {
            printf("ERROR: Unable to close file `%s'.\n", inputFile);
            exit(1);
        }

        printf("Num of Options: %d\n", numOptions);
        printf("Num of Runs: %d\n", NUM_RUNS);

#define PAD 256
#define LINESIZE 64

        buffer = (fptype *)malloc(5 * numOptions * sizeof(fptype) + PAD);
        sptprice = (fptype *)(((unsigned long long)buffer + PAD) & ~(LINESIZE - 1));
        strike = sptprice + numOptions;
        rate = strike + numOptions;
        volatility = rate + numOptions;
        otime = volatility + numOptions;

        buffer2 = (int *)malloc(numOptions * sizeof(fptype) + PAD);
        otype = (int *)(((unsigned long long)buffer2 + PAD) & ~(LINESIZE - 1));

        for (i = 0; i < numOptions; i++)
        {
            otype[i] = (::data[i].OptionType == 'P') ? 1 : 0;
            sptprice[i] = ::data[i].s;
            strike[i] = ::data[i].strike;
            rate[i] = ::data[i].r;
            volatility[i] = ::data[i].v;
            otime[i] = ::data[i].t;
        }

        printf("Size of data: %ld\n", numOptions * (sizeof(OptionData) + sizeof(int)));
        pascal_stop(0);
        auto tf = chrono::high_resolution_clock::now();
        double ep = chrono::duration_cast<chrono::milliseconds>(tf - ti).count();
        rapl.open(rapl_path);
        rapl >> enf;
        rapl.close();

        if( (enf-eni) > en_min ){
            p1_discard.push_back( vector<int>{f, c} );
            continue;
        }
        t_time = "(" + to_string(ep) + ", ";
        t_energy = "(" + to_string((enf - eni)/1e6) + ", ";

        f = phase[1][0];
        c = phase[1][1];
        t_phase+= "(" + to_string(f) + ", " + to_string(c) + "), ";
        cmd = "./cpu_control.py --cores " + to_string(c) + " --gov userspace --freq " + to_string(f);
        system(cmd.c_str());

        rapl.open(rapl_path);
        rapl >> eni;
        rapl.close();
        ti = chrono::high_resolution_clock::now();
        pascal_start(1);
        {
            int tid = 0;
            omp_set_num_threads(nThreads);
            bs_thread(&tid);
        }
        pascal_stop(1);
        tf = chrono::high_resolution_clock::now();
        ep = chrono::duration_cast<chrono::milliseconds>(tf - ti).count();
        rapl.open(rapl_path);
        rapl >> enf;
        rapl.close();

        if( (enf-eni) > en_min ){
            p2_discard.push_back( vector<int>{phase[0][0], phase[0][1], phase[1][0], phase[1][1]} );
            continue;
        }

        t_time += to_string(ep) + ", ";
        t_energy += to_string((enf - eni)/1e6) + ", ";

        f = phase[2][0];
        c = phase[2][1];
        t_phase+= "(" + to_string(f) + ", " + to_string(c) + "))";
        cmd = "./cpu_control.py --cores " + to_string(c) + " --gov userspace --freq " + to_string(f);
        system(cmd.c_str());

        rapl.open(rapl_path);
        rapl >> eni;
        rapl.close();
        ti = chrono::high_resolution_clock::now();
        pascal_start(2);
        //Write prices to output file
        file = fopen(outputFile, "w");
        if (file == NULL)
        {
            printf("ERROR: Unable to open file `%s'.\n", outputFile);
            exit(1);
        }
        rv = fprintf(file, "%i\n", numOptions);
        if (rv < 0)
        {
            printf("ERROR: Unable to write to file `%s'.\n", outputFile);
            fclose(file);
            exit(1);
        }
        for (i = 0; i < numOptions; i++)
        {
            rv = fprintf(file, "%.18f\n", prices[i]);
            if (rv < 0)
            {
                printf("ERROR: Unable to write to file `%s'.\n", outputFile);
                fclose(file);
                exit(1);
            }
        }
        rv = fclose(file);
        if (rv != 0)
        {
            printf("ERROR: Unable to close file `%s'.\n", outputFile);
            exit(1);
        }

#ifdef ERR_CHK
        printf("Num Errors: %d\n", numError);
#endif
        free(::data);
        free(prices);

        pascal_stop(2);
        tf = chrono::high_resolution_clock::now();
        ep = chrono::duration_cast<chrono::milliseconds>(tf - ti).count();
        rapl.open(rapl_path);
        rapl >> enf;
        rapl.close();
        t_energy += to_string( (enf - eni)/1e6 ) + ")";
        t_time += to_string(ep) + ")";

        auto ttf = chrono::high_resolution_clock::now();
        rapl.open(rapl_path);
        rapl >> entf;
        rapl.close();
        ep = chrono::duration_cast<chrono::milliseconds>(ttf - tti).count();
        table << nphase++ << "," << t_phase << "," << t_time 
                << "," << t_energy << "," << ep << "," << (entf-enti)/1e6 << endl;
        if( (entf-enti)<en_min ){
            en_min= entf-enti;
        }
    }

    return 0;
}
