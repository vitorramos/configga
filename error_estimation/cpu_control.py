#!/usr/bin/env python3
from cpufreq import cpuFreq
import time

def check_governors(governor):
    """
    Check if the governos are correct
    :return: bool
    """
    online_governors = cpu.get_governors()
    governos = [v == governor for v in online_governors.values()]
    return online_governors, all(governos)

def check_number_of_cores(core):
    """
    Check if the number of cores are correct
    :return: bool
    """
    cpus_online = cpu.get_online_cpus()
    return cpus_online, len(cpus_online) == core

def set_governor_retry(governor, max_retries=10):
    """
    Set governor with retry

    :max_retries: maximum number of tries
    :return: bool
    """
    for i in range(max_retries):
        cpu.set_governors(governor)
        time.sleep(2)
        governors, govs_ok = check_governors(governor)
        if govs_ok:
            print("Governors ", governors)
            break
        else:
            print("ERROR: Problem trying to set the governors")
            print("ERROR: Governors ", governors)
            print(f"ERROR: Retry {i}/10")
    else:
        raise Exception("Cant set governors cpu")


def disable_cores_retry(core, max_retries=10):
    """
    Disable cores with retry

    :core: number of cores to set
    :max_retries: maximum number of tries
    :return: bool
    """
    to_reset = cpu._cpuFreq__get_ranges("present")
    for i in range(max_retries):
        if core > len(cpus_avail):
            cpu.enable_cpu(to_reset[:core])
        else:
            cpu.disable_cpu(cpus_avail[core:])
        cpus_online, cpus_ok = check_number_of_cores(core)
        if cpus_ok:
            print("Cpus online", cpus_online)
            break
        else:
            print("ERROR: Problem trying to set the cpu online")
            print("ERROR: CPUs online ", cpus_online)
            print("ERROR: Number of cores ", core)
            print(f"ERROR: Retry {i}/10")
    else:
        raise Exception("Cant disable cpu")


import argparse

parser = argparse.ArgumentParser(description="CPU control")
parser.add_argument("--freq", type=int, help="set frequencies")
parser.add_argument("--cores", type=int, help="set number of cores")
parser.add_argument("--gov", type=str, help="set governors")
parser.add_argument("--reset", action='store_true', help="reset cpu")

args = parser.parse_args()

cpu = cpuFreq()
cpus_avail = cpu.get_online_cpus()

if args.reset:
    print("Reseting")
    cpu.reset()

if args.gov:
    print("Setting governors", args.gov)
    set_governor_retry(args.gov)

if args.cores:
    print("Setting number of cores", args.cores)
    disable_cores_retry(args.cores)

if args.freq:
    print("Setting frequencies", args.freq)
    cpu.set_frequencies(args.freq)


# import itertools
# f= [3000000, 2800000, 2600000, 2400000, 2200000, 2000000, 1800000, 1600000, 1400000, 1200000]
# c= [1,2,3,4]
# d= list(itertools.product(f,c))
# f= list(itertools.product(d,d,d))
# for a in f:
#     print(a)