#include <vector>
#include <unordered_map>
#include <tuple>
#include <sstream>
#include <regex>
#include <random>
#include <map>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <chrono>
#include <algorithm>
#include <omp.h>

#include "json.hpp"

using namespace std;
using json = nlohmann::json;

struct pascal
{
    // cores, freq, input, rep -> ipmi_power
    struct info
    {
        double *ipmi_time;
        double *ipmi_power;
        size_t samples;
        double start_time, stop_time;
    };

    size_t len;
    info **data;
    string *keys;
};

vector<string> split(string str, char delim = ' ')
{
    vector<string> cont;
    stringstream ss(str);
    string token;

    while (getline(ss, token, delim))
    {
        cont.push_back(token);
    }
    return cont;
}

inline double get_timeslice_energy_edge(pascal::info *data, double start, double stop)
{
    double t1 = data->start_time + (data->stop_time - data->start_time) * start;
    double t2 = data->start_time + (data->stop_time - data->start_time) * stop;
    double en = 0;
    int i, v1, v2;
    double *aux_time = data->ipmi_time;
    double *aux_power = data->ipmi_power;

    for (i = 0; i < data->samples; i++)
        if (aux_time[i] >= t1)
            break;

    v1 = (i == 0) ? 0 : i - 1;
    for (i = v1; i < data->samples; i++)
        if (aux_time[i] >= t2)
            break;

    v2 = (i == 0) ? 0 : i - 1;
    if (v1 == v2)
        return (t2 - t1) * aux_power[v1];

    en = (aux_time[v1 + 1] - t1) * aux_power[v1] + (t2 - aux_time[v2]) * aux_power[v2];
    for (i = v1 + 1; i < v2; i++)
        en += (aux_time[i + 1] - aux_time[i]) * aux_power[i];

    return en;
}

inline double get_timeslice_energy(pascal::info *data, double start, double stop)
{
    double mean = 0;
    double total = 0;
    double t1 = data->start_time + (data->stop_time - data->start_time) * start;
    double t2 = data->start_time + (data->stop_time - data->start_time) * stop;
    double aux;
    double *aux_time = data->ipmi_time;
    double *aux_power = data->ipmi_power;

    for (int ii = 0; ii < data->samples; ii++)
    {
        aux = aux_time[ii];
        if ((aux >= t1) & (aux <= t2))
        {
            mean += aux_power[ii];
            total++;
        }
    }

    if (total == 0)
        return 0;

    mean = mean / total * (t2 - t1);
    return mean;
}

inline double objective(pascal &data, vector<double> &x)
{
    double en, min_en = -1, total_en = 0;
    string min_conf;

    for (int i = 0; i < x.size() - 1; i++)
    {
        min_en = -1;
        for (int ii = 0; ii < data.len; ii++)
        {
            en = get_timeslice_energy_edge(data.data[ii], x[i], x[i + 1]);
            if (en >= 0 && (min_en == -1 || en < min_en))
            {
                min_en = en;
                min_conf = data.keys[ii];
            }
        }

        if (min_en == -1)
            return INFINITY;

        total_en += min_en;
    }

    return total_en;
}

inline double objective_max(pascal &data, vector<double> &x)
{
    double en, max_en = -1, total_en = 0;
    string min_conf;

    for (int i = 0; i < x.size() - 1; i++)
    {
        max_en = -1;
        for (int ii = 0; ii < data.len; ii++)
        {
            en = get_timeslice_energy_edge(data.data[ii], x[i], x[i + 1]);

            if (en >= 0 && (max_en == -1 || en > max_en))
            {

                max_en = en;
                min_conf = data.keys[ii];
            }
        }
        if (max_en == -1)
            return INFINITY;

        total_en += max_en;
    }

    return total_en;
}

class GA
{

    int pop_size = 1e4;
    int n_phases = 3;
    int n_iter = 100;
    int cur_iter = 0;
    int nstart = 0;

    pascal data;
    vector<vector<double>> pop;
    vector<double> fit;
    vector<size_t> idx;

    double min_en = -1, avr_fit = 0, cnt = 0;
    string appname, save_name;
public:
    void load_data(string name)
    {

        appname = name;
        json json_data;
        ifstream f(name);
        f >> json_data;
        int last_input = json_data["config"]["arguments"].get<vector<string>>().size();

        regex re("^.*;.*;" + to_string(last_input) + ";1");
        int cnt = 0;
        int MAX_SIZE = 9999;

        data.data = new pascal::info *[MAX_SIZE];
        data.keys = new string[MAX_SIZE];
        for (auto &el : json_data["data"].items())
        {
            if (regex_match(el.key(), re))
            {
                pascal::info *a = new pascal::info;
                auto aux = el.value()["sensors"]["ipmi"].get<vector<vector<double>>>();
                a->ipmi_power = new double[aux.size()];
                a->ipmi_time = new double[aux.size()];
                a->samples = aux.size();
                for (int ii = 0; ii < aux.size(); ii++)
                {
                    a->ipmi_power[ii] = aux[ii][0];
                    a->ipmi_time[ii] = aux[ii][1];
                }

                a->start_time = a->ipmi_time[0];
                a->stop_time = a->ipmi_time[aux.size() - 1];
                data.data[cnt] = a;
                data.keys[cnt] = el.key();
                data.len = ++cnt;

                if (cnt >= MAX_SIZE)
                {
                    MAX_SIZE *= 2;
                    pascal::info **new_buff_info = new pascal::info *[MAX_SIZE];
                    memcpy(new_buff_info, data.data, cnt * sizeof(pascal::info *));
                    delete[] data.data;
                    data.data = new_buff_info;
                    string *new_buff_key = new string[MAX_SIZE];
                    memcpy(new_buff_key, data.keys, cnt * sizeof(string));
                    // delete[] data.keys;
                    data.keys = new_buff_key;
                }
            }
        }
    }

    void initialize(int n_phases, int pop_size, int n_iter)
    {
        this->n_phases = n_phases;
        this->pop_size = pop_size;
        this->n_iter = n_iter;

        pop.clear();
        fit.clear();
        idx.clear();

        random_device rd;
        default_random_engine gen(rd());
        uniform_real_distribution<double> dist(0, 1);

        pop.resize(pop_size);
        for (int i = 0; i < pop_size; i++)
        {
            pop[i].push_back(0);
            for (int j = 0; j < n_phases - 2; j++)
                pop[i].push_back(dist(gen));

            pop[i].push_back(1);
            sort(pop[i].begin(), pop[i].end());
        }

        fit.resize(pop_size);
        idx.resize(pop_size);
        iota(idx.begin(), idx.end(), 0);
    }

    GA(string appname, string save_name)
    {
        this->save_name = save_name;
        load_data(appname);
        initialize(n_phases, pop_size, n_iter);
    }

    void compute()
    {
        cur_iter = 0;
        for (int i = 0; i < n_iter; i++)
        {
            eval();
        }

        ofstream save(save_name, ofstream::app);
        save << appname << ", " << n_phases << ", " << nstart << ", "
             << fit[idx[0]] << ", " << fit[idx.back()] << ", " << avr_fit << ", |";

        for (auto &j : pop[idx[0]])
            save << j << " ";

        save << "|" << endl;
        save.close();

        cout << appname << ", " << n_phases << ", " << nstart << ", "
             << fit[idx[0]] << ", " << fit[idx.back()] << ", " << avr_fit << ", |";

        for (auto &j : pop[idx[0]])
            cout << j << " ";

        cout << "|" << endl;
    }

    void eval()
    {
        auto t1 = chrono::high_resolution_clock::now();

        min_en = -1;
        avr_fit = 0;
        cnt = 0;

        double avr_fit_aux = 0, cnt_aux = 0;
        #pragma omp parallel for reduction(+ : avr_fit_aux) reduction(+ : cnt_aux)
        for (int i = 0; i < pop.size(); i++)
        {
            fit[i] = objective(data, pop[i]);
            if (fit[i] != INFINITY)
            {
                avr_fit_aux = (avr_fit_aux * cnt_aux + fit[i]) / (cnt_aux + 1);
                cnt_aux += 1;
            }
        }

        avr_fit = avr_fit_aux / omp_get_max_threads();
        cnt = cnt_aux;

        sort(idx.begin(), idx.end(), [this](size_t i1, size_t i2)
             {
                 return this->fit[i1] < this->fit[i2];
             });

        nstart = pop.size() - 1;

        while (nstart > 0 && fit[idx[nstart]] == INFINITY)

            nstart--;

        random_device rd;
        default_random_engine gen(rd());
        uniform_int_distribution<int> rep_dist(0, nstart);
        uniform_int_distribution<int> rep_ind(1, n_phases - 2);
        uniform_real_distribution<double> dist(0, 1);

        float elitism = 0.9, mutation = 0.1, new_ind = 0.1;
        vector<vector<double>> new_pop(pop_size * elitism + 1);

        for (int i = 0; i < pop_size * elitism; i++)
        {

            int i1 = rep_dist(gen);
            int i2 = rep_dist(gen);

            vector<double> aux;
            vector<double> *p1 = &pop[idx[i1]];
            vector<double> *p2 = &pop[idx[i2]];

            aux.insert(aux.end(), p1->begin(), p1->begin() + p1->size() / 2);
            aux.insert(aux.end(), p2->begin() + p1->size() / 2, p2->end());

            // cout << p1->size() << " " << p2->size() << " " << aux.size() << endl;

            sort(aux.begin(), aux.end());
            new_pop[i] = aux;
        }

        for (int i = 0; i < pop_size * elitism; i++)
        {
            pop[idx[pop_size - i - 1]] = new_pop[i];
        }

        for (int i = 0; i < pop_size * mutation; i++)
        {

            int i1 = rep_dist(gen);
            int b = rep_ind(gen);
            pop[i1][b] = dist(gen);
            sort(pop[i1].begin(), pop[i1].end());
        }

        for (int i = 0; i < pop_size * new_ind; i++)
        {
            for (int j = 1; j < n_phases - 1; j++)

                pop[idx[pop_size - i - 1]][j] = dist(gen);
            sort(pop[idx[pop_size - i - 1]].begin(), pop[idx[pop_size - i - 1]].end());
        }

        auto t2 = chrono::high_resolution_clock::now();

        cur_iter++;
    }
};

int main(int argc, char **argv)
{
    string path = argv[1];
    string name = path.substr(path.find_last_of("/") + 1);

    cout << "res_" + name + ".csv" << endl;
    GA x(path, "res_/" + name + ".csv");
    for (int i = 3; i < 100; i++)
    {
        x.initialize(i, 1e4, 300);
        x.compute();
    }
}
