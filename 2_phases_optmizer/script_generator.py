#!/usr/bin/python
import glob

script = """#!/bin/bash
#SBATCH --job-name=phases_{0}
#SBATCH --cpus-per-task=8
#SBATCH --hint=compute_bound
#SBATCH --time=24:00:00
#SBATCH --output={0}.out
#SBATCH --error={0}.err
#SBATCH --partition=full

export OMP_NUM_THREADS=8
./ga {1}
"""

for f in glob.glob("data/*.json"):
    sname = f.split("completo")[1][1:-5]
    print(sname, "script_{0}.sh".format(sname))
    with open("script_{0}.sh".format(sname), "w+") as g:
        g.write(script.format(sname,f))
