 ![Flow diagram](diagram.png) Fluxogram

# Requirements
 - python3
   - pascalanalyzer
   - performance_features
 - c++ compiler (with c++11 support)
   - libomp-dev
   - build-essential
   - linux-headers-`uname -r`

# Collecting the data

```bash
cd data_collection
pascalanalyzer -c 1:32 -f 1300000:100000:2300000 -i "i1,...,in" ./app -o results/app.json
# For parsec applications
# BASE_PATH must point to the correct parsec suite application setup
python collect_data.py BASE_PATH
# see results/fixed
```

# Running the phase optimizer
```bash
cd find_phases
make
mkdir res_ # output folder
```
 - ## Locally
    ```bash
    ./ga app.json
    ```
 - ## Cluster (slurm)
    ```bash
    cp *.json data/
    python script_generator.py
    chmod +x launcher.sh
    ./launcher.sh
    ```

 - ## Result
   The result is a CSV as follows (see res_/phases.csv):
   ```csv
   app.json, 3, 9999, 9931.13, 10083.5, 9998.05, |0 0.533783 1 |
   ...
   app.json, 99, 9999, 9665.32, 9790.69, 9687.72, |0 ... 1|
   ```
   The columns are the application name, number of phases, GA population size, min energy, max energy, average energy, and min energy phase division.

# Runtime driver
This folder contains an POC kernel driver that can use the optmized phased to control the cpu frequency, build with kernel 5.19 API
```bash
cd governor
make -j4
sudo insmod cpufreq_fingerprint.ko
echo fingerprint > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
```
The driver uses an equation that takes performance counters as inputs and computes the next frequency. This equation can be obtained from the optimized phases of the application using the fingerprint method (see find_fingerprint).
TODO: implement a attribute on the driver that allow us to provid an application profile ex:
```bash
echo "p1 p2 p3 p4 XXX" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_profile
```
Where p1...pn are the application phase (in percentage) and XXX is the total number of instructions.

# Error estimation
This folder contains an application with builtin phase switch that we can use as proof of concept and estimate the error of the phase optmizer
```bash
cd error_estimation
make -j4
```
   - blackscholes : collect energy samples from manually selected phases.
   - blackscholes_combinations : Run all possible phase combinations of frequencies and cores.
   - blackscholes_combinations_best : Run all possible phase combinations of frequencies and cores skipping when the consumption is higher than the minimal one so far.
   - blackscholes_combinations_prune : Run all possible phase combinations of frequencies and cores skipping phases that consumes more than the best phase so far.